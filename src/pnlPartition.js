import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
import { Table, Switch, Space } from 'antd';

const columns = [
    
    {
        title: 'Running Jobs',
        dataIndex: 'runningJob',
        key: 'runningJob',
        width: '10px',
        
    },
    {
        title: 'Waiting Jobs',
        dataIndex: 'waitingJobs',
        key: 'waitingJobs',
        width: '10px',
       
    },

];

const data = [
    {
        key: '1',
        name: '     ',
        runningJob: 32,
        waitingJobs: '6',
        tags: ['nice', 'developer'],
    },
    
    ]
// rowSelection objects indicates the need for row selection
const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows);
    },
};

function PnlPartition(props) {
    const [checkStrictly, setCheckStrictly] = React.useState(false);
   // console.log("props.runningJob:" +props.runningJob+"  , props.waitingJobs :" +props.waitingJobs);
   // debugger
    data[0].runningJob = props.runningJob;
    data[0].waitingJobs = props.waitingJobs;
    console.log();
    return (

        <div>
           
            <Table
                pagination={false}
                columns={columns}
                
                dataSource={data}
            />
        </div>

    );
}
;
export default PnlPartition