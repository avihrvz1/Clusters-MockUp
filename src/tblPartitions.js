import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
import { Table, Switch, Space } from 'antd';
import TabMQpriority from './tabMQpriority.js';
import $ from 'jquery';

const columns = [
    {
        title: 'Partition Name',
        dataIndex: 'name',
        key: 'name',
        render: text => <a>{text}</a>,
        width: '130px',
    },
    {
        title: 'Running Jobs',
        dataIndex: 'age',
        key: 'age',
        width: '130px',
    },
    {
        title: 'Waiting Jobs',
        dataIndex: 'address',
        key: 'address',
        width: '130px',
    },
   
];

const data = [
    {
        key: '1',
        name: 'Partition 1',
        age: 32,
        address: '6',
        tags: ['nice', 'developer'],
    },
    {
        key: '2',
        name: 'Partition 2',
        age: 42,
        address: '7',
        tags: ['loser'],
    },
    {
        key: '3',
        name: 'Partition 3',
        age: 32,
        address: '4',
        tags: ['cool', 'teacher'],
    },
    {
        key: '4',
        name: 'Partition 4',
        age: 32,
        address: '4',
        tags: ['cool', 'teacher'],
    },
    {
        key: '5',
        name: 'Partition 5',
        age: 32,
        address: '4',
        tags: ['cool', 'teacher'],
    },
    {
        key: '6',
        name: 'Partition 6',
        age: 32,
        address: '4',
        tags: ['cool', 'teacher'],
    },
    {
        key: '7',
        name: 'Partition 7',
        age: 32,
        address: '4',
        tags: ['cool', 'teacher'],
    },
    {
        key: '8',
        name: 'Partition 8',
        age: 32,
        address: '4',
        tags: ['cool', 'teacher'],
    },
];


const dataInsideServers = [
    {
        key: '1',
        name: 'Partition 1',
        age: 'Low ,High, Many Files',
        address: '6',
        tags: ['nice', 'developer'],
    },
    {
        key: '2',
        name: 'Partition 2',
        age: 'Low',
        address: '7',
        tags: ['loser'],
    },
    {
        key: '3',
        name: 'Partition 3',
        age: 'High, Many Files',
        address: '4',
        tags: ['cool', 'teacher'],
    },
    {
        key: '4',
        name: 'Partition 4',
        age: 'Low ,High',
        address: '4',
        tags: ['cool', 'teacher'],
    },
    {
        key: '5',
        name: 'Partition 5',
        age: 'High, Many Files',
        address: '4',
        tags: ['cool', 'teacher'],
    },
    {
        key: '6',
        name: 'Partition 6',
        age: 'Low ,High',
        address: '4',
        tags: ['cool', 'teacher'],
    },
    {
        key: '7',
        name: 'Partition 7',
        age: 'High, Many Files',
        address: '4',
        tags: ['cool', 'teacher'],
    },
    {
        key: '8',
        name: 'Partition 8',
        age: 'Low , Many Files',
        address: '4',
        tags: ['cool', 'teacher'],
    },
];

// rowSelection objects indicates the need for row selection
const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows);
    },
};

var divStyle2 = {
    padding: "10px 20px 10px 0px",
   // background: "#f0f0f0"
   
  };

var divStyle = {
    padding: "10px 20px 0px 10px",
    background: "#f0f0f0"
   
  };
function TablePartitions(props) {
    const [checkStrictly, setCheckStrictly] = React.useState(false);
    const isInsideServer = props.isInsideServer;


    let itemsids = ["Running Jobs","Waiting Jobs"];
    
    let filteredcolumns = columns.filter(function (currentElement) {
        
        return !itemsids.includes(currentElement.title);
    });
    filteredcolumns.push(

        {
            title: 'Complexity',
            dataIndex: 'age',
            key: 'age',
            width: '130px',
        }

    );
   
    return (

        <div>
           
           {isInsideServer ?<h4 style={divStyle2} >Server 1 is used in the following partitions:</h4> : <span class="tblTitle" >Partitions</span>}
            {   isInsideServer ?  
                    <Table style={divStyle}

                
                        columns={filteredcolumns}
                                                              
                        dataSource={dataInsideServers}
                    />
            :
            <Table

                
                columns={columns}
                
                expandable={{
                    expandedRowRender: record => <div class="insideTbl">  <TabMQpriority /></div>,
                }}
        
                dataSource={data}
             />
            
            
            
            }


        </div>

    );
}
export default TablePartitions;

//ReactDOM.render((<TabMQpriority />, document.getElementById('root'));