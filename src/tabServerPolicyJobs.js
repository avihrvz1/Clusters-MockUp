import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
import { Table, Switch, Tabs,Radio,Space } from 'antd';
import TblTabQueueSeverData from './tblTabQueueSeverData.js';
import TblTabQueuePoliciesData from './tblTabQueuePoliciesData.js';
import PnlPartition from './pnlPartition.js';
const { TabPane } = Tabs;

class TabServerPolicyJobs extends React.Component {
    state = { size: 'small' };

    onChange = e => {
        this.setState({ size: e.target.value });
    };

    render() {
        const { size } = this.state;
        return (
            
            <div class="innerTblInsideServerandPolicy">
                
                <Tabs defaultActiveKey="1" type="card" size={size}>
                    <TabPane tab="Servers" key="1">
                        <TblTabQueueSeverData />
                    </TabPane>
                    <TabPane tab="Policies" key="2">
                        <TblTabQueuePoliciesData />
                        
                    </TabPane>
                   
                </Tabs>
            </div>
        );
    }
}
export default TabServerPolicyJobs;