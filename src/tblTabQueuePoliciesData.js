import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
import { Table, Switch, Space } from 'antd';

const columns = [
    {
        title: 'Policy Name',
        dataIndex: 'name',
        key: 'name',
        width: '30%',
    },
    {
        title: 'Something',
        dataIndex: 'age',
        key: 'age',
        width: '30%',
    },
    {
        title: 'Something',
        dataIndex: 'address',
        width: '30%',
        key: 'address',
    },
];

const data = [
    {
        key: 1,
        name: 'Server 1',
        age: 60,
        address: 'Data',

    },
    {
        key: 2,
        name: 'Server 2',
        age: 32,
        address: 'Data',
    },
];

// rowSelection objects indicates the need for row selection
const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows);
    },
};

function TblTabQueuePoliciesData() {
    const [checkStrictly, setCheckStrictly] = React.useState(false);
    return (

        <div>
            

            <Table
                columns={columns}

                dataSource={data}
            />
        </div>

    );
}
export default TblTabQueuePoliciesData;