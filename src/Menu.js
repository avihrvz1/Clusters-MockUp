import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Layout   } from 'antd';
import { Menu, Button } from 'antd';
import {
    AppstoreOutlined,
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    PieChartOutlined,
    DesktopOutlined,
    ContainerOutlined,
    MailOutlined,
} from '@ant-design/icons';
import 'antd/dist/antd.css';
import './index.css';
import {
   
    CalendarOutlined,
    
    SettingOutlined,
    LinkOutlined,

} from '@ant-design/icons';

import TableServers from './tblServers.js';
import TablePartitions from './tblPartitions.js';
import TestNode from './testNode.js';


const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;

class RouterApp extends Component {

    state = {
        collapsed: false,
    };

    toggleCollapsed = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
             
            <Router>
                <Layout style={{ minHeight: '100vh' }}>

                    <Sider
                        >
                        <div className="logo" />
                      
                        <Menu
                            defaultSelectedKeys={['1']}
                            defaultOpenKeys={['sub1']}
                            mode="inline"
                            theme="dark"
                            inlineCollapsed={this.state.collapsed}
                        >
                       
                            <Menu.Item key="1" icon={<MailOutlined />}>
                              
                                <span>Partitions</span>
                                <Link to="/TablePartitions" />
                            </Menu.Item>
                            <Menu.Item key="2" icon={<CalendarOutlined />}>
                               
                                <span >Servers</span>
                                <Link to="/TableServers" />
                            </Menu.Item>
                            <Menu.Item key="2" icon={<CalendarOutlined />}>
                               
                                <span >Test</span>
                                <Link to="/TestNode" />
                            </Menu.Item>


                        </Menu>
                    </Sider>
                    <Layout>
                        <Header style={{ background: '#fff', padding: 0, paddingLeft: 16 }}>
                            <img class="resecImg" src="https://resec.tech/_nuxt/img/logo.9006f72.svg" alt="Italian Trulli"/>
                        </Header>
                        <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280 }}>
                            <Route exact path="/" component={TablePartitions} />
                            <Route path="/TableServers" component={TableServers} />
                            <Route path="/TablePartitions" component={TablePartitions} />
                            <Route path="/TestNode" component={TestNode} />
                        </Content>
                        <Footer style={{ background: '#fff', padding: 0, paddingLeft: 16 }} >Footer</Footer>
                    </Layout>

                </Layout>
            </Router>
        );
    }
}


export default RouterApp;