import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
import { Table, Switch, Space } from 'antd';
import TblPartitions from './tblPartitions.js';

const columns = [
    {
        title: 'Server Name',
        dataIndex: 'name',
        key: 'name',
        width: '50%',
        align: 'center'
    },
    {
        title: 'Status',
        dataIndex: 'age',
        key: 'age',
        width: '50%',
        align: 'center'
    },
   
];

const data = [
    {
        key: 1,
        name: 'Server 1',
        age: "O.K",
        address: '1',
        Complexity: '2',
     },
     {
           key: 13,
         name: 'Server 2',
         age: "O.K",
          address: '1',
         Complexity: '3',
     },
        
            
       
       
];

// rowSelection objects indicates the need for row selection
const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows);
    },
};

function TableServers() {
    const [checkStrictly, setCheckStrictly] = React.useState(false);
    return (

        <div style={{padding: 10}} >
            <span class="tblTitle" >Servers</span>

            <Table
                columns={columns}
                expandable={{
                    expandedRowRender: record => <div class="insideTbl">  <TblPartitions isInsideServer="true" /></div>,
                }}
                dataSource={data}
            />
        </div>

    );
}
export default TableServers;