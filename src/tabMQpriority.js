import { Tabs, Radio, Space } from 'antd';

import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
import PnlPartition from './pnlPartition.js';

import TabServerPolicyJobs from './tabServerPolicyJobs.js';

const { TabPane } = Tabs;

class TabMQpriority extends React.Component {
    state = {
        tabPosition: 'left',
    };

    changeTabPosition = e => {
        this.setState({ tabPosition: e.target.value });
    };
    /*<Space style={{ marginBottom: 24 }}>
                    Tab position:
                    <Radio.Group value={tabPosition} onChange={this.changeTabPosition}>
                        <Radio.Button value="top">top</Radio.Button>
                        <Radio.Button value="bottom">bottom</Radio.Button>
                        <Radio.Button value="left">left</Radio.Button>
                        <Radio.Button value="right">right</Radio.Button>
                    </Radio.Group>
                </Space>*/
    render () {
        const { tabPosition } = this.state;
        return (
            <>
               
               
                 <Tabs tabPosition={tabPosition}>
                    <TabPane tab="Low Complexity" key="1">
                        <div class="styleTabQueueData" > < PnlPartition waitingJobs="1" runningJob="2" />
                            <TabServerPolicyJobs /></div>
                    </TabPane>
                    <TabPane tab="High Complexity" key="2">
                        <div class="styleTabQueueData" > < PnlPartition waitingJobs="2" runningJob="3" />
                            <TabServerPolicyJobs /></div>
                    </TabPane>
                    <TabPane tab="Many Files" key="3">
                        <div class="styleTabQueueData" >< PnlPartition waitingJobs="5" runningJob="6" />
                            <TabServerPolicyJobs /></div>
                    </TabPane>
                </Tabs>
            </>
        );
    }
}
export default TabMQpriority;
